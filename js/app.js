import * as THREE from 'three';
import imagesLoaded from 'imagesloaded';
import gsap from 'gsap';
import FontFaceObserver from 'fontfaceobserver';
import Scroll from './scroll';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js';
import fragment from './shaders/fragment.glsl'
import vertex from './shaders/vertex.glsl'
import testTexture from '../img/texture.jpg';
import barba from '@barba/core';
import ASScroll from '@ashthornton/asscroll'
import { EffectComposer } from 'three/examples/jsm/postprocessing/EffectComposer.js';
import { RenderPass } from 'three/examples/jsm/postprocessing/RenderPass.js';
import { ShaderPass } from 'three/examples/jsm/postprocessing/ShaderPass.js';


export default class Sketch{
    constructor(options){
        this.time = 0;
        this.container = options.dom;
        this.scene = new THREE.Scene();

        this.width = this.container.offsetWidth;
        this.height = this.container.offsetHeight;

        this.camera = new THREE.PerspectiveCamera( 30, this.width/this.height, 100, 2000 );
        this.camera.position.z = 600;

        this.camera.fov = 2*Math.atan( (this.height/2)/600 )* (180/Math.PI);

        this.renderer = new THREE.WebGLRenderer( { 
            antialias: true,
            alpha: true
        } );
        
        this.container.appendChild( this.renderer.domElement );

        this.controls = new OrbitControls( this.camera, this.renderer.domElement );

        this.images = [...document.querySelectorAll('img')];

        const fontOpen = new Promise(resolve => {
          new FontFaceObserver("Open Sans").load().then(() => {
            resolve();
          });
        });

        const fontPlayfair = new Promise(resolve => {
          new FontFaceObserver("Playfair Display").load().then(() => {
            resolve();
          });
        });

        // Preload images
        const preloadImages = new Promise((resolve, reject) => {
            imagesLoaded(document.querySelectorAll("img"), { background: true }, resolve);
        });

        let allDone = [fontOpen,fontPlayfair,preloadImages]
        this.currentScroll = 0;
        this.raycaster = new THREE.Raycaster();
        this.mouse = new THREE.Vector2();



        Promise.all(allDone).then(()=>{
            this.asscroll = new ASScroll({
                disableRaf: true
            });
    
            this.asscroll.enable({
                // horizontalScroll: !document.body.classList.contains('b-inside')
            })
            this.addImages();
            this.setPosition();
            this.addClickEvents();
         
            this.mouseMovement()
            this.resize()
           
            this.composerPass();
           
            this.render();
            this.barba()
            this.setupResize();

        })

        
    }
//zoom effect when click
     addClickEvents(){
        this.imageStore.forEach(i=>{
            i.img.addEventListener('click',()=>{
                let tl = gsap.timeline()
                .to(i.mesh.material.uniforms.uCorners.value,{
                    x:1,
                    duration: 0.4
                })
                .to(i.mesh.material.uniforms.uCorners.value,{
                    y:1,
                    duration: 0.4
                },0.1)
                .to(i.mesh.material.uniforms.uCorners.value,{
                    z:1,
                    duration: 0.4
                },0.2)
                .to(i.mesh.material.uniforms.uCorners.value,{
                    w:1,
                    duration: 0.4
                },0.3)
            })

            
        })
    }
//effect when scrolling
    composerPass(){
        this.composer = new EffectComposer(this.renderer);
        this.renderPass = new RenderPass(this.scene, this.camera);
        this.composer.addPass(this.renderPass);
  
        //custom shader pass
        var counter = 0.0;
        this.myEffect = {
            uniforms: {
              "tDiffuse": { value: null },
              "scrollSpeed": { value: null },
            },
            vertexShader: `
            varying vec2 vUv;
            void main() {
              vUv = uv;
              gl_Position = projectionMatrix 
                * modelViewMatrix 
                * vec4( position, 1.0 );
            }
            `,
            fragmentShader: `
            uniform sampler2D tDiffuse;
            varying vec2 vUv;
            uniform float scrollSpeed;
            void main(){
              vec2 newUV = vUv;
              float area = smoothstep(0.4,0.,vUv.y);
              area = pow(area,4.);
              newUV.x -= (vUv.x - 0.5)*0.1*area;
              gl_FragColor = texture2D( tDiffuse, newUV);
            //   gl_FragColor = vec4(area,0.,0.,1.);
            }
            `
          }
        this.customPass = new ShaderPass(this.myEffect);
        this.customPass.renderToScreen = true;
  
        this.composer.addPass(this.customPass);
      }
//barba transition that include zoom effect
barba(){
    this.animationRunning = false;
    let that = this;
    barba.init({
        
      transitions: [{
        name: 'from-home-transition',
        from: {
            namespace: ["home"]
        },
        leave(data) {
            that.animationRunning = true;
            that.asscroll.disable();
            return gsap.timeline()
                .to(data.current.container,{
                    opacity: 0.,
                    duration: 0.5
                })
        },
        enter(data) {
            that.asscroll = new ASScroll({
                containerElement: data.next.container.querySelector("[asscroll-container]")
            })
            that.asscroll.enable({
                newScrollElements: data.next.container.querySelector('.scroll-wrap')
            })
            return gsap.timeline()
            .from(data.next.container,{
                opacity: 0.,
                onComplete: ()=>{
                    that.container.style.visibility = "hidden";
                    that.animationRunning = false
                }
            })
        }
      },
      {
        name: 'from-inside-page-transition',
        from: {
            namespace: ["inside"]
        },
        leave(data) {
            that.asscroll.disable();
            
            return gsap.timeline()
                .to('.curtain',{
                    duration: 0.3,
                    y: 0
                })
                .to(data.current.container,{
                    opacity: 0.
                })
        },
        enter(data) {
            that.asscroll = new ASScroll({
                disableRaf: true,
                containerElement: data.next.container.querySelector("[asscroll-container]")
            })
            that.asscroll.enable({
                newScrollElements: data.next.container.querySelector('.scroll-wrap')
            })
            // cleeaning old arrays
            that.imageStore.forEach(m=>{
                that.scene.remove(m.mesh)
            })
            that.imageStore = []
            that.materials = []
            that.addImage();
            that.resize();
            that.addClickEvents()
            that.container.style.visibility = "visible";
            

            return gsap.timeline()
            .to('.curtain',{
                duration: 0.3,
                y: "-100%"
            })
            .from(data.next.container,{
                opacity: 0.
            })
        }
      }
    
      

    ]
    });
  }
//add click function
  addClickEvents(){
    this.imageStore.forEach(i=>{
        i.img.addEventListener('click',()=>{
            let tl = gsap.timeline()
            .to(i.mesh.material.uniforms.uCorners.value,{
                x:1,
                duration: 0.4
            })
            .to(i.mesh.material.uniforms.uCorners.value,{
                y:1,
                duration: 0.4
            },0.1)
            .to(i.mesh.material.uniforms.uCorners.value,{
                z:1,
                duration: 0.4
            },0.2)
            .to(i.mesh.material.uniforms.uCorners.value,{
                w:1,
                duration: 0.4
            },0.3)
        })
    })
}
//hover function
    mouseMovement(){
        

        window.addEventListener( 'mousemove', (event)=>{
            this.mouse.x = ( event.clientX / this.width ) * 2 - 1;
            this.mouse.y = - ( event.clientY / this.height ) * 2 + 1;

            // update the picking ray with the camera and mouse position
            this.raycaster.setFromCamera( this.mouse, this.camera );

            // calculate objects intersecting the picking ray
            const intersects = this.raycaster.intersectObjects( this.scene.children );

            if(intersects.length>0){
                // console.log(intersects[0]);
                let obj = intersects[0].object;
                obj.material.uniforms.hover.value = intersects[0].uv;
            }


        }, false );
    }

    setupResize(){
        window.addEventListener('resize',this.resize.bind(this));
    }

//resize the screen when different screen size
    resize(){
        this.width = this.container.offsetWidth;
        this.height = this.container.offsetHeight;
        this.renderer.setSize( this.width,this.height );
        this.camera.aspect = this.width/this.height;
        this.camera.updateProjectionMatrix();

        this.camera.fov = 2*Math.atan( (this.height/2)/600 ) * 180/Math.PI;

        this.materials.forEach(m=>{
            m.uniforms.uResolution.value.x = this.width;
            m.uniforms.uResolution.value.y = this.height;
        })
        
        // this.imageStore.forEach(i=>{
        //     let bounds = i.img.getBoundingClientRect();
        //     i.mesh.scale.set(bounds.width,bounds.height,1);
        //     i.top = bounds.top;
        //     i.left = bounds.left + this.currentScroll;
        //     i.width = bounds.width;
        //     i.height = bounds.height;

        //     i.mesh.material.uniforms.uQuadSize.value.x = bounds.width;
        //     i.mesh.material.uniforms.uQuadSize.value.y = bounds.height;

        //     i.mesh.material.uniforms.uTextureSize.value.x = bounds.width;
        //     i.mesh.material.uniforms.uTextureSize.value.y = bounds.height;
        // })

    }

//addImages to the html page
    addImages(){
        
        this.material = new THREE.ShaderMaterial({
            uniforms:{
                time: {value:1.0},
                uProgress: { value: 0 },
                uTexture: {value: null},
                uTextureSize: {value: new THREE.Vector2(100,100)},
                hover:{value:new THREE.Vector2(0.5,0.5)},
                hoverState:{value:0},
                uCorners: {value: new THREE.Vector4(0,0,0,0)},
                uResolution: { value: new THREE.Vector2(this.width,this.height) },
                uQuadSize: { value: new THREE.Vector2(450,450) }
            },
            side:THREE.DoubleSide,
            fragmentShader: fragment,
            vertexShader: vertex,
            
        })

        this.materials = []

        this.imageStore = this.images.map(img=>{
            let bounds = img.getBoundingClientRect()

            let geometry = new THREE.PlaneBufferGeometry(bounds.width,bounds.height,1,1);
            console.log(bounds.width,bounds.height)
            let texture = new THREE.Texture(img);
            texture.needsUpdate = true;
    
            let material = this.material.clone();

            img.addEventListener('mouseenter',()=>{
                gsap.to(material.uniforms.hoverState,{
                    duration:1,
                    value:1
                })
            })
            img.addEventListener('mouseout',()=>{
                gsap.to(material.uniforms.hoverState,{
                    duration:1,
                    value:0
                })
                
            })

            this.materials.push(material)

            material.uniforms.uTexture.value = texture;

            let mesh = new THREE.Mesh(geometry,material);

            this.scene.add(mesh)


            return {
                img: img,
                mesh: mesh,
                top: bounds.top,
                left: bounds.left,
                width: bounds.width,
                height: bounds.height
            }
        })

        console.log(this.imageStore);

    }
//set the content position when scrolling
    setPosition(){
        this.imageStore.forEach(o=>{
            o.mesh.position.y = this.asscroll.currentPos-o.top + this.height/2 - o.height/2;
            o.mesh.position.x = o.left - this.width/2 + o.width/2;
        })
    }

//render function
    render(){
        this.time+=0.05;

        // this.scroll.render();
        // this.currentScroll = this.scroll.scrollToRender;
        // this.customPass.uniforms.scrollSpeed.value=this.scroll.speedTarget;
        this.asscroll.update()
        
        this.setPosition();


        // this.material.uniforms.time.value = this.time;

        this.materials.forEach(m=>{
            m.uniforms.time.value = this.time;
        })
        this.composer.render();
        //this.renderer.render( this.scene, this.camera );
        window.requestAnimationFrame(this.render.bind(this));
    }
}

new Sketch({
    dom: document.getElementById('container')
});

